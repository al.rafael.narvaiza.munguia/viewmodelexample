package com.iesportada.semipresencial.viewmodelexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import android.view.View;
import com.iesportada.semipresencial.viewmodelexample.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityMainBinding binding;
    private MainActivityViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);

        binding.buttonAdd.setOnClickListener(this);

        viewModel= new ViewModelProvider(this).get(MainActivityViewModel.class);

        //seteamos el valor del TV con la data del viewModel en cada llamada al onCreate.
        binding.TextViewNumber.setText(String.valueOf(viewModel.getNumber()));
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        binding.TextViewNumber.setText(String.valueOf(viewModel.addNumber()));
    }
}