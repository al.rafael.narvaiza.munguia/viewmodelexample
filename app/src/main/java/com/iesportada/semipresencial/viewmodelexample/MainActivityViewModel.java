package com.iesportada.semipresencial.viewmodelexample;

import androidx.lifecycle.ViewModel;

public class MainActivityViewModel extends ViewModel {

    private int number = 0;


    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int addNumber(){
        number++;
        return number;
    }
}
